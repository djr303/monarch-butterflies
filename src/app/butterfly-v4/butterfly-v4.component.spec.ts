import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButterflyV1Component } from './butterfly-v1.component';

describe('ButterflyV1Component', () => {
  let component: ButterflyV1Component;
  let fixture: ComponentFixture<ButterflyV1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButterflyV1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButterflyV1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
