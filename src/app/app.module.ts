import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ButterflyV1Component } from './butterfly-v1/butterfly-v1.component';
import { ButterflyV2Component } from './butterfly-v2/butterfly-v2.component';
import { ButterflyV3Component } from './butterfly-v3/butterfly-v3.component';
import { ButterflyV4Component } from './butterfly-v4/butterfly-v4.component';
import { ButterflyV5Component } from './butterfly-v5/butterfly-v5.component';
import { ButterflyV6Component } from './butterfly-v6/butterfly-v6.component';
import { ButterflyV7Component } from './butterfly-v7/butterfly-v7.component';
import { ButterflyV8Component } from './butterfly-v8/butterfly-v8.component';
import { ButterflyV9Component } from './butterfly-v9/butterfly-v9.component';
import { ButterflyV10Component } from './butterfly-v10/butterfly-v10.component';
import { ButterflyV11Component } from './butterfly-v11/butterfly-v11.component';
import { ButterflyV12Component } from './butterfly-v12/butterfly-v12.component';
import { ButterflyV13Component } from './butterfly-v13/butterfly-v13.component';
import { ButterflyV14Component } from './butterfly-v14/butterfly-v14.component';
import { ButterflyV15Component } from './butterfly-v15/butterfly-v15.component';
import { ButterflyV16Component } from './butterfly-v16/butterfly-v16.component';
import { ButterflyV17Component } from './butterfly-v17/butterfly-v17.component';
import { ButterflyV18Component } from './butterfly-v18/butterfly-v18.component';
import { ButterflyV19Component } from './butterfly-v19/butterfly-v19.component';
import { ButterflyV20Component } from './butterfly-v20/butterfly-v20.component';
import { ButterflyV21Component } from './butterfly-v21/butterfly-v21.component';
import { ButterflyV22Component } from './butterfly-v22/butterfly-v22.component';
import { ButterflyV23Component } from './butterfly-v23/butterfly-v23.component';
import { ButterflyV24Component } from './butterfly-v24/butterfly-v24.component';

@NgModule({
  declarations: [
    AppComponent,
    ButterflyV1Component,
    ButterflyV2Component,
    ButterflyV3Component,
    ButterflyV4Component,
    ButterflyV5Component,
    ButterflyV6Component,
    ButterflyV7Component,
    ButterflyV8Component,
    ButterflyV9Component,
    ButterflyV10Component,
    ButterflyV11Component,
    ButterflyV12Component,
    ButterflyV13Component,
    ButterflyV14Component,
    ButterflyV15Component,
    ButterflyV16Component,
    ButterflyV17Component,
    ButterflyV18Component,
    ButterflyV19Component,
    ButterflyV20Component,
    ButterflyV21Component,
    ButterflyV22Component,
    ButterflyV23Component,
    ButterflyV24Component,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
